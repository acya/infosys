/* #ifndef DEBUG */

/* #define INITLOGGER(name, level) */
/* #define LOGVERBOSE(...) */
/* #define LOGINFO(...) */
/* #define LOGWARN(...) */
/* #define LOGDEBUG(...) */
/* #define LOGERROR(...) (fprintf(stderr, __VA_ARGS__)) */
/* #define LOGPANIC(...) (fprintf(stderr, __VA_ARGS__)) */
/* #define CRASH(...)\ */
/* 	fprintf(stderr, __VA_ARGS__);\ */
/* 	perror("");\ */
/* 	exit(EXIT_FAILURE) */
/* #define _CRASH(...)\ */
/* 	fprintf(stderr, __VA_ARGS__);\ */
/* 	perror("");\ */
/* 	_exit(EXIT_FAILURE) */
/* #define THREAD_CRASH(...)\ */
/* 	fprintf(stderr, __VA_ARGS__);\ */
/* 	perror("");\ */
/* 	pthread_exit(NULL) */

/* #else */

#define MSG_MAX_SIZE 4096
#define PREFIX_MAX_SIZE 4096
#define LOGNAME_MAX_SIZE 256

#define LEVEL_VERBOSE 0
#define LEVEL_INFO 1
#define LEVEL_DEBUG 2
#define LEVEL_WARN 3
#define LEVEL_ERROR 4
#define LEVEL_PANIC 5

#define INITLOGGER(name, level) (logger_init(name, level))
#define LOGVERBOSE(...) (logging(LEVEL_VERBOSE, __VA_ARGS__))
#define LOGINFO(...) (logging(LEVEL_INFO, __VA_ARGS__))
#define LOGWARN(...) (logging(LEVEL_WARN, __VA_ARGS__))
#define LOGDEBUG(...) (logging(LEVEL_DEBUG, __VA_ARGS__))
#define LOGERROR(...) (logging(LEVEL_ERROR, __VA_ARGS__))
#define LOGPANIC(...) (logging(LEVEL_PANIC, __VA_ARGS__))
#define RETURN_ERROR(rval, ...)\
	LOGERROR(__VA_ARGS__);\
	LOGERROR(strerror(errno));\
	return rval
#define EXIT_ERROR(...)\
	LOGERROR(__VA_ARGS__);\
	LOGPANIC(strerror(errno));\
	exit(EXIT_FAILURE)
#define _EXIT_ERROR(...)\
	LOGERROR(__VA_ARGS__);\
	LOGPANIC(strerror(errno));\
	_exit(EXIT_FAILURE)
#define PTHREAD_EXIT_ERROR(...)\
	LOGERROR(__VA_ARGS__);\
	LOGPANIC(strerror(errno));\
	pthread_exit(NULL)

#define COL_NORMAL "\x1B[0m"
#define COL_RED "\x1B[31m"
#define COL_GREEN "\x1B[32m"
#define COL_YELLOW "\x1B[33m"
#define COL_BLUE "\x1B[34m"
#define COL_MAGENTA "\x1B[35m"
#define COL_CYAN "\x1B[36m"
#define COL_WHITE "\x1B[37m"
#define COL_RESET "\033[0m"

#include <errno.h>

void logging(int, const char *, ...);
void logger_init(const char *, int);

/* #endif */
