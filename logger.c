#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include "logger.h"

static pthread_mutex_t mutex;
static struct {
	int minLevel;
	char name[LOGNAME_MAX_SIZE];
} logger;

void logging(int level, const char *msg, ...) {
	if (pthread_mutex_lock(&mutex)) {
		fprintf(stderr, "LOGGER: Cannot lock mutex. Aborting.\n");
		_exit(EXIT_FAILURE);
	}

	char fullmsg[MSG_MAX_SIZE];
	char prefix[PREFIX_MAX_SIZE];
	va_list args;
	va_start(args, msg);
	vsprintf(fullmsg, msg, args);
	va_end(args);

	if (level >= logger.minLevel) {
		int pid = (int)getpid();
		int tid = syscall(SYS_gettid);

		switch (level) {
		case LEVEL_VERBOSE:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_BLUE "VERBOSE" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		case LEVEL_INFO:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_CYAN "INFO" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		case LEVEL_DEBUG:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_GREEN "DEBUG" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		case LEVEL_WARN:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_YELLOW "WARN" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		case LEVEL_ERROR:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_RED "ERROR" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		case LEVEL_PANIC:
			snprintf(
				prefix, PREFIX_MAX_SIZE,
				"[P%d T%d] [" COL_MAGENTA "PANIC" COL_RESET "] [%s] ",
				pid, tid, logger.name);
			break;
		default:
			fprintf(stderr, "LOGGER: Did not expect the level %d\n", level);
			return;
		}

		fprintf(stderr, "%s%s\n", prefix, fullmsg);
	}

	if (pthread_mutex_unlock(&mutex)) {
		fprintf(stderr, "LOGGER: Cannot unlock mutex. Aborting.\n");
		_exit(EXIT_FAILURE);
	}
}

void logger_init(const char *name, int minLevel) {
	strncpy(logger.name, name, LOGNAME_MAX_SIZE);
	logger.minLevel = minLevel;

	pthread_mutexattr_t mutexAttr;
	if (pthread_mutexattr_init(&mutexAttr) ||
		pthread_mutexattr_settype(&mutexAttr, PTHREAD_MUTEX_RECURSIVE) ||
		pthread_mutex_init(&mutex, &mutexAttr) ||
		pthread_mutexattr_destroy(&mutexAttr)) {
		fprintf(stderr, "LOGGER: Cannot initialize mutex. Aborting.\n");
		_exit(EXIT_FAILURE);
	}
}
