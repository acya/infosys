#include "infosys-server.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	if (argc != 1) {
		fprintf(stderr, "Usage: %s \n The programme didn't get any argument.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	infosysServerInit(INFOSYS_DEFAULT_SERVERNAME);
	infosysServerStart();
}
