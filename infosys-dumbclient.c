#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "infosys-client.h"
#include "logger.h"

void scanRequest();
void printAnswer();

int main(int argc, char *argv[]) {
	INITLOGGER("Client", 0);
	if (argc != 1) {
		fprintf(stderr, "Usage: %s \n The programme didn't get any argument.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	if (infosysLinkToServer(INFOSYS_DEFAULT_SERVERNAME) != OK) {
		exit(EXIT_FAILURE);
	}
	scanRequest();
	printAnswer();
}

void scanRequest() {
	while (1) {
		printf("%s\n", "Plese input the code of your request's type:");
		printf("%s\n", "1-> processus informations");
		printf("%s\n", "2-> user informations by UID");
		printf("%s\n", "3-> user informations by username");
		int type;
		scanf("%i", &type);

		switch (type) {
		case 1:
			printf("%s\n", "Please input a PID");
			int pid;
			scanf("%i", &pid);
			if (infosysAskPID(pid) != OK) {
				LOGERROR("Failed");
				exit(EXIT_FAILURE);
			}
			break;
		case 2:
			printf("%s\n", "Please input a UID");
			int uid;
			scanf("%i", &uid);
			if (infosysAskUID(uid) != OK) {
				LOGERROR("Failed");
				exit(EXIT_FAILURE);
			}
			break;
		case 3:
			printf("%s\n", "Please input a username");
			char username[LOGIN_NAME_MAX];
			scanf("%s", username);
			if (infosysAskUsername(username) != OK) {
				LOGERROR("Failed");
				exit(EXIT_FAILURE);
			}
			break;
		default:
			printf("%s\n", "Type unknown. Try again.");
			continue;
		}
		break;
	}
}

void printAnswer() {
	char answer[INFOSYS_ANSWER_CONTENT_SIZE];
	infosysFetchAnswer(answer);
	printf("%s\n", answer);
}
