#include "infosys.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>

char *getInfoByPid(pid_t);
int StartsWith(const char *, const char *);

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \n The programme use one argument.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char *arg  = malloc(sizeof(char) * INFOSYS_FIFOREQUESTS_LENGTH);
	int pidInt = atoi(argv[1]);
	pid_t pid  = (pid_t)pidInt;
	arg = getInfoByPid(pid);
	printf("%s\n", arg);
	exit(EXIT_SUCCESS);
}

int StartsWith(const char *a, const char *b) {
	if (strncmp(a, b, strlen(b)) == 0)
		return 1;
	return 0;
}

char *getInfoByPid(pid_t pid) {
	char  *reponse     = malloc(sizeof(char) * INFOSYS_ANSWER_CONTENT_SIZE);
	char file_name[25] = "/proc/";
	char str[10];
	sprintf(str, "%d", pid);
	strcat(file_name, str);
	strcat(file_name, "/status");
	FILE *fp;
	fp = fopen(file_name, "r");
	if (fp == NULL) {
		strcpy(reponse, "Aucun processus avec ce pid\n");
		return reponse;
	}
	char buf[250];
	while (fgets(buf, sizeof(buf), fp))
		if (StartsWith(buf, "PPid") ||
			StartsWith(buf, "Tgid") ||
			StartsWith(buf, "State") ||
			StartsWith(buf, "Name") ||
			StartsWith(buf, "Uid") ||
			StartsWith(buf, "Gid") ||
			StartsWith(buf, "Groupes")) {
			strcat(reponse, buf);
		}
	fclose(fp);
	return reponse;
}
