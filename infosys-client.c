#include "infosys-common.h"

#define INDEX_BUFFER INFOSYS_ANSWER_CONTENT_SIZE * fifoAnswer->head

static struct infosysFifoRequests_t *fifoRequests;
static struct infosysFifoAnswer_t *fifoAnswer;
static struct {
	bool cleanupSet;
	bool linkedToServer;
	char serverName[INFOSYS_SERVERNAME_MAX_LENGTH];
	bool linkedToAnswer;
	pid_t answerOwner;
} state;

infosysStatus_t setCleanup();
infosysStatus_t createAnswerShm();

infosysStatus_t infosysLinkToServer(const char *serverName) {
	if (!state.cleanupSet) {
		infosysStatus_t s;
		if ((s = setCleanup()) != OK) {
			LOGERROR("Failed to set cleanup safeguards");
			return s;
		}
	}
	if (state.linkedToServer) {
		LOGERROR("Already linked to a server");
		return UNSUPPORTED;
	}

	strncpy(state.serverName, serverName, INFOSYS_SERVERNAME_MAX_LENGTH);
	char shmName[NAME_MAX];
	requestsShmName(state.serverName, shmName);
	LOGINFO("Opening FIFO's SHM under the name %s", shmName);
	int shmFd = shm_open(shmName, O_RDWR, S_IRUSR | S_IWUSR);
	if (shmFd == -1) {
		RETURN_ERROR(CRASHED, "FIFO's shm_open client side aborted");
	}
	if (ftruncate(shmFd, INFOSYS_FIFOREQUESTS_SIZE) == -1) {
		if (close(shmFd) == -1) {
			EXIT_ERROR("close");
		}
		RETURN_ERROR(CRASHED, "ftruncate");
	}

	LOGINFO("Projecting FIFO's SHM");
	char *shmPtr = mmap(
		NULL, INFOSYS_FIFOREQUESTS_SIZE,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, shmFd, 0);
	if (shmPtr == MAP_FAILED) {
		if (close(shmFd) == -1) {
			EXIT_ERROR("close");
		}
		RETURN_ERROR(CRASHED, "mmap");
	}
	if (close(shmFd) == -1) {
		EXIT_ERROR("close");
	}

	LOGINFO("Setting FIFO's global variable");
	fifoRequests = (struct infosysFifoRequests_t *)shmPtr;

	LOGINFO("Successfully linked to server via SHM %s", shmName);
	state.linkedToServer = true;
	return OK;
}

infosysStatus_t createAnswerShm() {
	state.linkedToAnswer = false;
	pid_t pid = getpid();

	char shmName[PATH_MAX];
	answerShmName(pid, state.serverName, shmName);

	LOGINFO("Creating the answer's SHM under the name %s", shmName);
	int shmFd = shm_open(shmName, O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
	if (shmFd == -1) {
		RETURN_ERROR(CRASHED, "Client's shm_open %s aborted.", shmName);
	}
	if (ftruncate(shmFd, INFOSYS_FIFOANSWER_SIZE) == -1) {
		RETURN_ERROR(CRASHED, "ftruncate");
	}

	LOGINFO("Projecting the answer's SHM");
	char *shmPtr = mmap(
		NULL, INFOSYS_FIFOANSWER_SIZE, PROT_READ | PROT_WRITE,
		MAP_SHARED, shmFd, 0);
	if (shmPtr == MAP_FAILED) {
		RETURN_ERROR(CRASHED, "mmap");
	}

	LOGINFO("Setting the answer's global variable");
	fifoAnswer = (struct infosysFifoAnswer_t *)shmPtr;
	close(shmFd);

	LOGINFO("Initializing the answer's structure");
	if (sem_init(&fifoAnswer->mutex, 1, 1) == -1) {
		RETURN_ERROR(CRASHED, "sem_init");
	}
	if (sem_init(&fifoAnswer->empty, 1, INFOSYS_FIFOANSWER_LENGTH) == -1) {
		RETURN_ERROR(CRASHED, "sem_init");
	}
	if (sem_init(&fifoAnswer->full, 1, 0) == -1) {
		RETURN_ERROR(CRASHED, "sem_init");
	}
	fifoAnswer->head  = 0;
	fifoAnswer->queue = 0;
	LOGINFO("Successfully created SHM for client %d", pid);
	state.linkedToAnswer = true;
	state.answerOwner    = pid;
	return OK;
}

infosysStatus_t sendRequest(struct infosysRequest_t request) {
	LOGINFO("Sending request");
	if (!state.linkedToServer) {
		LOGWARN("Cannot send request: Not linked to server");
		return UNSUPPORTED;
	}
	if (!state.linkedToAnswer || state.answerOwner != getpid()) {
		infosysStatus_t s;
		if ((s = createAnswerShm()) != OK) {
			LOGERROR("Could not create SHM for client %d", getpid());
			return s;
		}
	}

	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		RETURN_ERROR(CRASHED, "clock_gettime");
	}
	ts.tv_sec += INFOSYS_SERVER_TIMEOUT_SEC;
	int s;
	LOGINFO("Entering critical section");
	LOGINFO("Client sem_timedwait on fifoRequests.empty");
	while ((s = sem_timedwait(&fifoRequests->empty, &ts)) == -1 && errno == EINTR)
		continue;
	if (s == -1) {
		if (errno == ETIMEDOUT) {
			RETURN_ERROR(TIMEDOUT, "Server timed out.");
		} else {
			RETURN_ERROR(CRASHED, "sem_timedwait");
		}
	}

	LOGINFO("Client sem_wait on fifoRequests.mutex");
	if (sem_wait(&fifoRequests->mutex) == -1) {
		RETURN_ERROR(CRASHED, "sem_wait");
	}

	LOGINFO("Putting request info FIFO");
	fifoRequests->buffer[fifoRequests->head] = request;
	fifoRequests->head                       = (fifoRequests->head + 1) % INFOSYS_FIFOREQUESTS_LENGTH;

	LOGINFO("Client sem_post on fifoRequests.mutex");
	if (sem_post(&fifoRequests->mutex) == -1) {
		RETURN_ERROR(CRASHED, "sem_post");
	}

	LOGINFO("Client sem_post on fifoRequests.full");
	if (sem_post(&fifoRequests->full) == -1) {
		RETURN_ERROR(CRASHED, "sem_post");
	}
	LOGINFO("Left critical section");

	return OK;
}

infosysStatus_t infosysAskPID(pid_t pid) {
	struct infosysRequest_t request;
	request.pidClient = getpid();
	request.type      = PID;
	request.pid       = pid;
	return sendRequest(request);
}

infosysStatus_t infosysAskUID(uid_t uid) {
	struct infosysRequest_t request;
	request.pidClient = getpid();
	request.type      = UID;
	request.uid       = uid;
	return sendRequest(request);
}

infosysStatus_t infosysAskUsername(const char *username) {
	struct infosysRequest_t request;
	request.pidClient = getpid();
	request.type      = USERNAME;
	strncpy(request.username, username, LOGIN_NAME_MAX);
	return sendRequest(request);
}

infosysStatus_t infosysFetchAnswer(char *answer) {
	LOGINFO("Fetching answer");
	if (!state.linkedToAnswer) {
		LOGWARN("Cannot fetch answer: No valid shm");
		return UNSUPPORTED;
	}
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == -1) {
		RETURN_ERROR(CRASHED, "clock_gettime");
	}
	ts.tv_sec += INFOSYS_SERVER_TIMEOUT_SEC;
	int s;
	LOGVERBOSE("Entering critical section");
	LOGVERBOSE("Client sem_timedwait on fifoAnswer.full");
	while ((s = sem_timedwait(&fifoAnswer->full, &ts)) == -1 && errno == EINTR)
		continue;
	if (s == -1) {
		if (errno == ETIMEDOUT) {
			RETURN_ERROR(TIMEDOUT, "Server timed out.");
		} else {
			RETURN_ERROR(CRASHED, "sem_timedwait");
		}
	}
	LOGVERBOSE("Client sem_wait on fifoAnswer.mutex");
	if (sem_wait(&fifoAnswer->mutex) == -1) {
		RETURN_ERROR(CRASHED, "sem_wait");
	}
	LOGINFO("Copying answer's content");
	strncpy(answer, &fifoAnswer->buffer[INDEX_BUFFER], INFOSYS_ANSWER_CONTENT_SIZE);

	LOGVERBOSE("Client sem_post on fifoAnswer.mutex");
	if (sem_post(&fifoAnswer->mutex) == -1) {
		RETURN_ERROR(CRASHED, "sem_post");
	}

	LOGVERBOSE("Client sem_post on fifoAnswer.empty");
	if (sem_post(&fifoAnswer->empty) == -1) {
		RETURN_ERROR(CRASHED, "sem_post");
	}
	LOGVERBOSE("Left critical section");

	return OK;
}

infosysStatus_t infosysUnlinkToServer() {
	LOGINFO("Unlinking to server");
	if (!state.linkedToServer) {
		LOGWARN("Not linked to a server yet");
		return UNSUPPORTED;
	}
	if (munmap(fifoRequests, INFOSYS_FIFOREQUESTS_SIZE) == -1) {
		_EXIT_ERROR("munmap");
	}
	fifoRequests = NULL;
	if (state.linkedToAnswer) {
		char shmName[PATH_MAX];
		answerShmName(state.answerOwner, state.serverName, shmName);
		if (shm_unlink(shmName) == -1) {
			if (errno != ENOENT) {
				_EXIT_ERROR("shm_unlin");
			}
			LOGWARN("SHM %s does not exist", shmName);
		}
		if (sem_destroy(&fifoAnswer->mutex) == -1) {
			_EXIT_ERROR("sem_destroy");
		}
		if (sem_destroy(&fifoAnswer->full) == -1) {
			_EXIT_ERROR("sem_destroy");
		}
		if (sem_destroy(&fifoAnswer->empty) == -1) {
			_EXIT_ERROR("sem_destroy");
		}
		if (munmap(fifoAnswer, INFOSYS_FIFOANSWER_SIZE) == -1) {
			_EXIT_ERROR("munmap");
		}
	}
	fifoAnswer           = NULL;
	state.linkedToAnswer = false;
	state.linkedToServer = false;
	return OK;
}

void cleanupAtExit() {
	LOGWARN("Exiting. Cleaning up.");
	infosysUnlinkToServer();
}

void cleanupSignal(int signum) {
	LOGWARN("Interrupted by signal of code %d. Cleaning up.", signum);
	infosysUnlinkToServer();
	_exit(EXIT_SUCCESS);
}

infosysStatus_t setCleanup() {
	INITLOGGER("Client", 0);
	LOGINFO("Initializing infosys client-side");
	LOGINFO("Registering cleanup functions");
	atexit(cleanupAtExit);
	signal(SIGINT, cleanupSignal);
	signal(SIGQUIT, cleanupSignal);
	signal(SIGTERM, cleanupSignal);
	signal(SIGHUP, cleanupSignal);
	state.cleanupSet = true;
	return OK;
}
