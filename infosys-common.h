#define INFOSYS_SHMNAME_PREFIX "infosys_"
#define INFOSYS_SERVERNAME_MAX_LENGTH 32
#define INFOSYS_FIFOREQUESTS_LENGTH 10
#define INFOSYS_FIFOREQUESTS_SIZE\
	(sizeof(struct infosysFifoRequests_t) +\
	 INFOSYS_FIFOREQUESTS_LENGTH*sizeof(struct infosysRequest_t))

#define INFOSYS_FIFOANSWER_LENGTH 1
#define INFOSYS_FIFOANSWER_SIZE\
	(sizeof(struct infosysFifoAnswer_t) +\
	 INFOSYS_FIFOANSWER_LENGTH*INFOSYS_ANSWER_CONTENT_SIZE)

#include <stdlib.h>
#include <stdbool.h>
// Strings and stuff
#include <stdio.h>
#include <string.h>
// SHM, Semaphores, Files
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <semaphore.h>
// Time
#include <time.h>
// Threads
#include <pthread.h>
// Wait
#include <wait.h>
// Signals
#include <signal.h>
// System types and constants
#include <sys/types.h>
#include <limits.h>

#include "infosys.h"
#define INFOSYS_SERVER_TIMEOUT_SEC (time_t) INFOSYS_TIMEOUT_SEC
#include "logger.h"

enum infosysTypeRequest_t {
	PID,
	UID,
	USERNAME
};

struct infosysRequest_t {
	enum infosysTypeRequest_t type;
	pid_t pidClient;
	union {
		pid_t pid;
		uid_t uid;
		char username[LOGIN_NAME_MAX];
	};
};

struct infosysFifoAnswer_t {
	sem_t empty;
	sem_t full;
	sem_t mutex;
	int sizeBuffer;
	int head;
	int queue;
	char buffer[];
};

struct infosysFifoRequests_t {
	sem_t empty;
	sem_t full;
	sem_t mutex;
	int sizeBuffer;
	int head;
	int queue;
	struct infosysRequest_t buffer[];
};

infosysStatus_t answerShmName(pid_t pid, const char *serverName, char *shmName);
infosysStatus_t requestsShmName(const char *serverName, char *shmName);
