#include "infosys.h"

infosysStatus_t infosysServerInit(const char * fifoName);
infosysStatus_t infosysServerStart();
infosysStatus_t infosysServerShutdown();
