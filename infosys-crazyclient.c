#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <ctype.h>
#include <time.h>
#include "infosys-client.h"
#include "logger.h"

void printAnswer();
int randr(unsigned int, unsigned int);
void lunchCommande();

int main(int argc, char *argv[]) {
	INITLOGGER("Client", 0);
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \n Use one number paramater \n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char *endp;
	int nbrFork = strtol(argv[1], &endp, 10);
	printf("%d\n", nbrFork);
	if (nbrFork < 2) {
		printf("%s\n", "Chose a number > 2");
		exit(EXIT_FAILURE);
	}
	if (infosysLinkToServer(INFOSYS_DEFAULT_SERVERNAME) != OK) {
		exit(EXIT_FAILURE);
	}
	for (int i = 0; i < nbrFork; i++) {
		switch (fork()) {
		case -1:
			LOGERROR("%s%d\n", "Avortement");
		case 0:
			printf("Child with %d pid lunching request of type %d \n", getpid(), 1);
			lunchCommande();
			exit(EXIT_SUCCESS);
		default:
			wait(NULL);
			break;
		}
	}
}

void lunchCommande() {
	int type;
	srand(time(NULL));
	type = rand() % 4;
	char userName[LOGIN_NAME_MAX];
	strcpy(userName, "root");
	switch (type) {
	case 1:
		infosysAskPID(0001);
		printAnswer();
	case 2:
		infosysAskUID(1000);
		printAnswer();
	case 3:

		infosysAskUsername(userName);
		printAnswer();
	}
}

void printAnswer() {
	char answer[INFOSYS_ANSWER_CONTENT_SIZE];
	infosysFetchAnswer(answer);
	printf("%s\n", answer);
}
