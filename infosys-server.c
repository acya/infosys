#include "infosys-common.h"

#define INDEX_BUFFER INFOSYS_ANSWER_CONTENT_SIZE * fifoAnswer->head

static struct infosysFifoRequests_t *fifoRequests;
static struct {
	bool cleanupSet;
	bool initialized;
	char serverName[INFOSYS_SERVERNAME_MAX_LENGTH];
} state;

infosysStatus_t setCleanup();
infosysStatus_t listenRequest(struct infosysRequest_t *);
infosysStatus_t spawnRequestHandler(struct infosysRequest_t *);
void startCommand(struct infosysRequest_t *);

infosysStatus_t infosysServerInit(const char *serverName) {
	if (!state.cleanupSet) {
		int s;
		if ((s = setCleanup()) != OK) {
			LOGERROR("Failed to set cleanup safeguards");
			return s;
		}
	}

	if (state.initialized) {
		LOGWARN("Server already initialized. Aborting.");
		return UNSUPPORTED;
	}

	strncpy(state.serverName, serverName, INFOSYS_SERVERNAME_MAX_LENGTH);
	char shmName[NAME_MAX];
	requestsShmName(state.serverName, shmName);
	LOGINFO("Creating requests' SHM under the name %s", shmName);
	int shmFd = shm_open(
		shmName,
		O_RDWR | O_CREAT | O_EXCL,
		S_IRUSR | S_IWUSR);
	if (shmFd == -1) {
		RETURN_ERROR(CRASHED, "FIFO's shm_open server side aborted.");
	}
	if (ftruncate(shmFd, INFOSYS_FIFOREQUESTS_SIZE) == -1) {
		if (close(shmFd) == -1) {
			EXIT_ERROR("close");
		}
		RETURN_ERROR(CRASHED, "ftruncate");
	}

	LOGINFO("Projecting FIFO's SHM");
	char *shm_ptr = mmap(
		NULL, INFOSYS_FIFOREQUESTS_SIZE,
		PROT_READ | PROT_WRITE,
		MAP_SHARED, shmFd, 0);
	if (shm_ptr == MAP_FAILED) {
		RETURN_ERROR(CRASHED, "mmap");
	}
	if (close(shmFd) == -1) {
		EXIT_ERROR("close");
	}

	LOGINFO("Setting FIFO's global variable");
	fifoRequests = (struct infosysFifoRequests_t *)shm_ptr;

	LOGINFO("Initialize FIFO's structure");
	if (sem_init(&fifoRequests->mutex, 1, 1) == -1) {
		EXIT_ERROR("sem_init");
	}
	if (sem_init(&fifoRequests->empty, 1, INFOSYS_FIFOREQUESTS_LENGTH) == -1) {
		EXIT_ERROR("sem_init");
	}
	if (sem_init(&fifoRequests->full, 1, 0) == -1) {
		EXIT_ERROR("sem_init");
	}
	fifoRequests->head  = 0;
	fifoRequests->queue = 0;

	LOGINFO("Succesfully initialized server");
	state.initialized = true;
	return OK;
}

infosysStatus_t infosysServerShutdown() {
	LOGWARN("Server shutdown");
	if (!state.initialized) {
		LOGWARN("Server is not initialized. Cannot shutdown");
		return UNSUPPORTED;
	}
	if (sem_destroy(&fifoRequests->mutex) == -1) {
		_EXIT_ERROR("sem_destroy");
	}
	if (sem_destroy(&fifoRequests->full) == -1) {
		_EXIT_ERROR("sem_destroy");
	}
	if (sem_destroy(&fifoRequests->empty) == -1) {
		_EXIT_ERROR("sem_destroy");
	}
	char shmName[NAME_MAX];
	requestsShmName(state.serverName, shmName);
	if (shm_unlink(shmName) == -1) {
		if (errno != ENOENT) {
			_EXIT_ERROR("shm_unlink");
		}
		LOGWARN("%s does not exist", shmName);
	}
	return OK;
}

infosysStatus_t infosysServerStart() {
	LOGINFO("Starting server");
	if (!state.initialized) {
		LOGWARN("Cannot start server: not initialized");
		return UNSUPPORTED;
	}

	struct infosysRequest_t *request;
	infosysStatus_t s = OK;
	while (true) {
		request = (struct infosysRequest_t *)malloc(sizeof(struct infosysRequest_t));
		if ((s = listenRequest(request)) != OK) {
			LOGERROR("Could not listen to request. Crashing.");
			break;
		}
		if ((s = spawnRequestHandler(request)) != OK) {
			LOGERROR("Could not spawn request handler. Crashing.");
			break;
		}
		LOGINFO("Request for client %d should be handled. Looping.",
				request->pidClient);
	}
	// NOT REACHED
	LOGPANIC("Server crashed. Shutdown");
	infosysServerShutdown();
	return s;
}

void startCommand(struct infosysRequest_t *request) {
	LOGINFO("Starting command");
	char buf[LOGIN_NAME_MAX];
	int bufInt;
	switch (request->type) {
	case PID:
		bufInt = (int)request->pid;
		sprintf(buf, "%d", bufInt);
		LOGINFO("Execlp info_proc pid %s", buf);
		execlp("./infosys-info-pid", "infosys-info-pid", buf, NULL);
		LOGERROR("exec info_proc failed for client %d", request->pidClient);
		_exit(EXIT_FAILURE);
	case UID:
		bufInt = (int)request->uid;
		sprintf(buf, "%d", bufInt);
		LOGINFO("Execlp info_user_uid %s", buf);
		execlp("./infosys-info-uid", "infosys-info-uid", buf, NULL);
		LOGERROR("exec info_user_uid failed for client %d", request->pidClient);
		_exit(EXIT_FAILURE);
	case USERNAME:
		LOGINFO("Execlp info_user_username %s", request->username);
		execlp("./infosys-info-username", "infosys-info-username", request->username, NULL);
		LOGERROR("exec info_user_username failed for client %d", request->pidClient);
		_exit(EXIT_FAILURE);
	default:
		LOGERROR("typeRequest unknown for client %d. Aborting.", request->pidClient);
		_exit(EXIT_FAILURE);
	}
}

void sendAnswer(int pidClient, int readFd) {
	char shmName[PATH_MAX];
	answerShmName(pidClient, state.serverName, shmName);

	LOGINFO("Opening the answer's SHM under the name %s", shmName);
	int shmFd = shm_open(shmName, O_RDWR, S_IRUSR | S_IWUSR);
	if (shmFd == -1) {
		PTHREAD_EXIT_ERROR("Client's shm_open %s aborted.", shmName);
	}
	if (ftruncate(shmFd, INFOSYS_FIFOANSWER_SIZE) == -1) {
		PTHREAD_EXIT_ERROR("ftruncate");
	}

	LOGINFO("Projecting the answer's SHM of name %s", shmName);
	char *shmPtr = mmap(
		NULL, INFOSYS_FIFOANSWER_SIZE, PROT_READ | PROT_WRITE,
		MAP_SHARED, shmFd, 0);
	if (shmPtr == MAP_FAILED) {
		PTHREAD_EXIT_ERROR("mmap");
	}
	struct infosysFifoAnswer_t *fifoAnswer = (struct infosysFifoAnswer_t *)shmPtr;
	close(shmFd);

	LOGVERBOSE("Entering critical section, server side.");
	LOGVERBOSE("Server sem_wait on fifoAnswer.empty");
	if (sem_wait(&fifoAnswer->empty) == -1) {
		EXIT_ERROR("sem_wait fifoAnswer.empty");
	}
	LOGVERBOSE("Server sem_wait on fifoAnswer.mutex");
	if (sem_wait(&fifoAnswer->mutex) == -1) {
		EXIT_ERROR("sem_wait fifoAnswer.mutex");
	}

	LOGINFO("Reading the pipe into the answer's content for client %d", pidClient);
	int r;
	if ((r = read(readFd, &fifoAnswer->buffer[INDEX_BUFFER], INFOSYS_ANSWER_CONTENT_SIZE)) == -1) {
		PTHREAD_EXIT_ERROR("read");
	}

	LOGVERBOSE("Server sem_post on fifoAnswer.mutex");
	if (sem_post(&fifoAnswer->mutex) == -1) {
		EXIT_ERROR("sem_post fifoAnswer.mutex");
	}
	LOGVERBOSE("Server sem_post on fifoAnswer.full");
	if (sem_post(&fifoAnswer->full) == -1) {
		EXIT_ERROR("sem_post fifoAnswer.full");
	}
	LOGVERBOSE("Kill thread created for client %d", pidClient);
	pthread_exit(NULL);
}

void *routineRequestHandler(void *arg) {
	struct infosysRequest_t *request = (struct infosysRequest_t *)arg;

	LOGVERBOSE("Creating pipe for the request of client %d", request->pidClient);
	int pipeFd[2];
	pipe(pipeFd);
	LOGVERBOSE("Forking for client %d", request->pidClient);
	switch (fork()) {
	case -1:
		PTHREAD_EXIT_ERROR("fork for client %d aborted", request->pidClient);
	case 0:
		close(pipeFd[0]);
		LOGVERBOSE("Redirect stdout into pipe in child for client %d", request->pidClient);
		dup2(pipeFd[1], STDOUT_FILENO);
		close(pipeFd[1]);
		LOGINFO("Start command for client %d", request->pidClient);
		startCommand(request);
	default:
		close(pipeFd[1]);
		LOGINFO("Send answer to %d in parent", request->pidClient);
		int pidClient = request->pidClient;
		free(request);
		if (wait(NULL) < 0) {
			PTHREAD_EXIT_ERROR("Failed to collect child process");
		}
		LOGVERBOSE("Zombie child killed");
		sendAnswer(pidClient, pipeFd[0]);
	}
	return NULL;
}

infosysStatus_t spawnRequestHandler(struct infosysRequest_t *request) {
	pthread_t th;
	LOGVERBOSE("Creating thread for client %d", request->pidClient);
	if (pthread_create(&th, NULL, routineRequestHandler, request) != 0) {
		RETURN_ERROR(CRASHED, "pthread_create");
	}
	return OK;
}

infosysStatus_t listenRequest(struct infosysRequest_t *request) {
	LOGINFO("Start listening requests");

	LOGVERBOSE("Entering critical section, server side.");
	LOGVERBOSE("Server sem_wait on fifoRequests.full");
	int s;
	while ((s = sem_wait(&fifoRequests->full)) == -1 && errno == EINTR) {
		LOGINFO("Interrupted while waiting for a request");
		continue;
	}
	if (s == -1) {
		EXIT_ERROR("sem_wait fifoRequests.full");
	}
	LOGVERBOSE("Server sem_wait on fifoRequests.mutex");
	if (sem_wait(&fifoRequests->mutex) == -1) {
		EXIT_ERROR("sem_wait fifoRequests.mutex");
	}

	LOGINFO("Copying request from FIFO");
	memcpy(request, &fifoRequests->buffer[fifoRequests->queue], sizeof(struct infosysRequest_t));
	fifoRequests->queue = (fifoRequests->queue + 1) % INFOSYS_FIFOREQUESTS_LENGTH;

	LOGVERBOSE("Server sem_post on fifo.mutex");
	if (sem_post(&fifoRequests->mutex) == -1) {
		EXIT_ERROR("sem_post fifoRequests.mutex");
	}
	LOGVERBOSE("Server sem_post on fifo.empty");
	if (sem_post(&fifoRequests->empty) == -1) {
		EXIT_ERROR("sem_post fifoRequests.empty");
	}
	return OK;
}

void cleanupAtExit() {
	LOGWARN("Exiting. Cleaning up.");
	infosysServerShutdown();
}

void cleanupSignal(int signum) {
	LOGWARN("Interrupted by signal of code %d. Cleaning up.", signum);
	infosysServerShutdown();
	_exit(EXIT_SUCCESS);
}

infosysStatus_t setCleanup() {
	INITLOGGER("Server", 0);
	LOGINFO("Registering cleanup functions");
	atexit(cleanupAtExit);
	signal(SIGINT, cleanupSignal);
	signal(SIGQUIT, cleanupSignal);
	signal(SIGTERM, cleanupSignal);
	signal(SIGHUP, cleanupSignal);
	state.cleanupSet = true;
	return OK;
}
