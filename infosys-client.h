#include "infosys.h"

infosysStatus_t infosysLinkToServer(const char *fifoName);
infosysStatus_t infosysAskPID(pid_t pid);
infosysStatus_t infosysAskUID(uid_t uid);
infosysStatus_t infosysAskUsername(const char *username);
infosysStatus_t infosysFetchAnswer(char *answer);
infosysStatus_t infosysUnlinkToServer();
