#include "infosys-common.h"

infosysStatus_t answerShmName(pid_t pid, const char *serverName, char *shmName) {
	snprintf(shmName, PATH_MAX, "%s%s_%d",
			 "/" INFOSYS_SHMNAME_PREFIX "client_", serverName, pid);
	return OK;
}

infosysStatus_t requestsShmName(const char *serverName, char *shmName) {
	snprintf(shmName, NAME_MAX, "%s%s",
			 "/" INFOSYS_SHMNAME_PREFIX "server_", serverName);
	return OK;
}
