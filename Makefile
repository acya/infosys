CC = gcc
CCFLAGS = -std=c11 -Wall -pthread -Wextra -Werror -pedantic -D_XOPEN_SOURCE=600
LDFLAGS = -pthread
LDLIBS = -lrt

HDR = logger.h infosys-common.h infosys.h
SRC = $(wildcard *.c)
OBJ = $(SOURCES:.c=.o)
BIN = $(addprefix infosys-,dumbserver dumbclient crazyclient info-pid info-uid info-username)

all: $(BIN)

infosys-dumbserver: infosys-dumbserver.o logger.o infosys-server.o infosys-common.o

infosys-dumbclient: infosys-dumbclient.o logger.o infosys-client.o infosys-common.o

infosys-crazyclient: infosys-crazyclient.o logger.o infosys-client.o infosys-common.o

infosys-info-pid: infosys-info-pid.o

infosys-info-uid: infosys-info-uid.o

infosys-info-username: infosys-info-username.o

infosys-dumbserver.o: infosys-dumbserver.c infosys-server.h  $(HDR)

infosys-dumbclient.o: infosys-dumbclient.c infosys-client.h  $(HDR)

infosys-crazyclient.o: infosys-crazyclient.c infosys-client.h  $(HDR)

%.o: %.c $(HDR)
	$(CC) $(CCFLAGS) -c $<

clean:
	$(RM) -f *.o $(BIN) 

style:
	uncrustify -c uncrustify.cfg --replace *.c
	$(RM) -f *.unc-backup~ *.unc-backup.md5~
