#include "infosys.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>

char *getInfoByUserName(char *);

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \n The programme use one argument.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char *arg = malloc(sizeof(char) * INFOSYS_FIFOREQUESTS_LENGTH);
	arg = getInfoByUserName(argv[1]);
	printf("%s\n", arg);
	exit(EXIT_SUCCESS);
}

char *getInfoByUserName(char *user) {
	char *reponse = malloc(sizeof(char) * INFOSYS_ANSWER_CONTENT_SIZE);
	struct passwd *pwd;
	if ((pwd = getpwnam(user)) == NULL)
		strcpy(reponse, "Unknown user");
	else {
		char buf[sizeof(int)];
		strcpy(reponse, "\n");
		strcat(reponse, "User ID: \t");
		int uid = (int)pwd->pw_gid;
		snprintf(buf, sizeof buf, "%d", uid);
		strcat(reponse, buf);
		strcat(reponse, "\n");
		strcat(reponse, "Group ID: \t");
		int pwGid = (int)pwd->pw_gid;
		snprintf(buf, sizeof buf, "%d", pwGid);
		strcat(reponse, buf);
		strcat(reponse, "\n");
		strcat(reponse, "User information: \t");
		strcat(reponse, pwd->pw_gecos);
		strcat(reponse, "\n");
		strcat(reponse, "Home directory: \t");
		strcat(reponse, pwd->pw_dir);
		strcat(reponse, "\n");
		strcat(reponse, "Shell program: \t");
		strcat(reponse, pwd->pw_shell);
	}
	return reponse;
}
