#include "infosys.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <pwd.h>

char *getInfoByUid(uid_t);

int main(int argc, char *argv[]) {
	if (argc != 2) {
		fprintf(stderr, "Usage: %s \n The programme use one argument.\n", argv[0]);
		exit(EXIT_FAILURE);
	}
	char *arg  = malloc(sizeof(char) * INFOSYS_FIFOREQUESTS_LENGTH);
	int uidInt = atoi(argv[1]);
	uid_t uid  = (uid_t)uidInt;
	arg = getInfoByUid(uid);
	printf("%s\n", arg);
	exit(EXIT_SUCCESS);
}

char *getInfoByUid(uid_t uid) {
	char *reponse = malloc(sizeof(char) * INFOSYS_ANSWER_CONTENT_SIZE);
	struct passwd *pwd;
	if ((pwd = getpwuid(uid)) == NULL)
		strcpy(reponse, "utilisateur inconnu");
	else {
		strcpy(reponse, "Username:\t");
		strcat(reponse, pwd->pw_name);
		strcat(reponse, "\n");
		strcat(reponse, "Group ID\t");
		int pwGid = (int)pwd->pw_gid;
		char buf[sizeof(int)];
		snprintf(buf, sizeof buf, "%d", pwGid);
		strcat(reponse, buf);
		strcat(reponse, "\n");
		strcat(reponse, "User information:\t");
		strcat(reponse, pwd->pw_gecos);
		strcat(reponse, "\n");
		strcat(reponse, "Home directory:\t");
		strcat(reponse, pwd->pw_dir);
		strcat(reponse, "\n");
		strcat(reponse, "Shell program:\t");
		strcat(reponse, pwd->pw_shell);
	}
	return reponse;
}
